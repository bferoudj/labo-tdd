import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.util.NoSuchElementException;

import static org.mockito.Mockito.*;


public class FifoTest {

    @Test public void testGivenEmptyListWhenIsEmptyThenEmpty() {

        LinkedList list = new LinkedList();
        Fifo fifo = new Fifo(list);

        assertTrue(fifo.isEmpty());
    }


    @Test public void testIsEmptyMockedList() {

        LinkedList mockedList = mock(LinkedList.class);
        Fifo fifo = new Fifo(mockedList);

        assertFalse(fifo.isEmpty());
        verify(mockedList, times(1)).isEmpty();

    }

    @Test public void testIsEmptyMockedStubList() {

        LinkedList mockedList = mock(LinkedList.class);
        when(mockedList.isEmpty()).thenReturn(true);

        Fifo fifo = new Fifo(mockedList);

        assertTrue(fifo.isEmpty());
    }

    @Test public void testMockedLinkedList() {
        LinkedList mockedList = mock(LinkedList.class);

        assertFalse(mockedList.isEmpty());
        mockedList.addToHead(3);
        assertFalse(mockedList.isEmpty());

        String data = mockedList.displayNode(42);

        assertEquals(data, null);
        assertEquals(mockedList.length(),0);

        verify(mockedList).displayNode(42);
    }

    @Test public void testStubMockedLinkedList() {
        LinkedList mockedList = mock(LinkedList.class);
        assertFalse(mockedList.isEmpty());

        when(mockedList.isEmpty()).thenReturn(true);
        when(mockedList.displayNode(42)).thenReturn("Mocked");

        assertTrue(mockedList.isEmpty());

        mockedList.addToHead(3);
        assertTrue(mockedList.isEmpty());

        String data_stub = mockedList.displayNode(42);
        String data_nostub = mockedList.displayNode(43);

        assertEquals(data_stub, "Mocked");
        assertEquals(data_nostub, null);
        assertEquals(mockedList.length(),0);
        verify(mockedList, atLeast(2)).isEmpty();
        verify(mockedList, atLeastOnce()).displayNode(42);
        verify(mockedList, times(1)).displayNode(43);
    }

    @Test public void testGivenEmptyListWhenEnqueueThenAddToHead() {
        // Arange
        LinkedList mockedList = mock(LinkedList.class);
        Fifo fifo = new Fifo(mockedList);

        // Act
        fifo.enqueue(3);

        // Assert
        verify(mockedList, times(1)).addToHead(3);
    }

    @Test public void testGivenEmptyListWhenEnqueueThenNotEmpty() {
        // Arange
        LinkedList mockedList = mock(LinkedList.class);
        when(mockedList.isEmpty()).thenReturn(true).thenReturn(false);
        Fifo fifo = new Fifo(mockedList);
        assertTrue(fifo.isEmpty());

        // Act
        fifo.enqueue(3);

        // Assert
        assertFalse(fifo.isEmpty());
    }

    @Test public void testGivenEmptyListWhenDequeueThenNoSuchElementExeption() {
        // Arange
        LinkedList mockedList = mock(LinkedList.class);
        when(mockedList.isEmpty()).thenReturn(true);

        // Act
        // Assert
        assertThrows(NoSuchElementException.class, () -> {
            Fifo fifo = new Fifo(mockedList);
            fifo.dequeue();
        });
    }
}

