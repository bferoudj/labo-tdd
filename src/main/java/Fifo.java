import java.util.NoSuchElementException;

public class Fifo {
    private LinkedList list;

    public Fifo(LinkedList l){
        list = l;
    }

    public boolean isEmpty() {
        return list.isEmpty();
    }

    public void enqueue(int data){
        list.addToHead(data);
    }

    public int dequeue() throws NoSuchElementException {
        throw(new NoSuchElementException("Empty List"));
    }
}
